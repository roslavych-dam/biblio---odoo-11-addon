*Disclaimer: this project is in Catalan.*

Small project to get used to an Odoo v11 addon's structure.

The specifications for the models were:
![](images/models.jpg)

The result for the models:
*  authors:
![](images/authors.jpg)
*  books:
![](images/books.jpg)
*  editorials:
![](images/editorials.jpg)

The relationship between an author and their books:
![](images/relationship-author-books.jpg)

The relationship among a book, its autors and its editorial:
*  on create:
![](images/relationship-new-book-author-editorial.jpg)
*  existing:
![](images/relationship-book-author-editorial.jpg)

The relationship between a book and its authors:
![](images/relationship-book-authors.jpg)

The relationship between an editorial and its books:
![](images/relationship-editorial-books.jpg)
