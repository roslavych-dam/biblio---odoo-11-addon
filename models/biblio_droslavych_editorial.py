from odoo import models, fields
class BiblioDroslavychEditorial(models.Model):
  _name = 'biblio_droslavych.editorial'
  nom = fields.Char(string='Nom', size=150, required=True)
  adreca = fields.Char(string='Adreça', size=300, required=True)
  telf = fields.Char(string='Telèfon', size=150)
  email = fields.Char(string='Correu electrònic', size=150, required=True)
  llibre_id = fields.One2many('biblio_droslavych.llibre', 'editorial_id', string='Llibres')