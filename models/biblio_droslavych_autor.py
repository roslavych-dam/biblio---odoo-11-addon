from odoo import models, fields
class BiblioDroslavychAutor(models.Model):
  _name = 'biblio_droslavych.autor'
  nom = fields.Char(string='Nom', size=150, required=True)
  cognom = fields.Char(string='Cognom', size=150, required=True)
  datanaix = fields.Date(string='Data naix.')
  telf = fields.Char(string='Telèfon', size=150)
  email = fields.Char(string='Correu electrònic', size=150)
  llibre_id = fields.Many2many('biblio_droslavych.llibre', string='Llibres')