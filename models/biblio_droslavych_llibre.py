from odoo import models, fields
class BiblioDroslavychLlibre(models.Model):
  _name = 'biblio_droslavych.llibre'
  codi = fields.Char(string='Codi', size=150, required=True)
  titol = fields.Char(string='Títol', size=150, required=True)
  pagines = fields.Integer(string='Nº pàgines')
  isbn = fields.Char(string='ISBN', size=150, required=True)
  descripcio = fields.Char(string='Descripció', size=300)
  autor_id = fields.Many2many('biblio_droslavych.autor', string='Autors')
  editorial_id = fields.Many2one('biblio_droslavych.editorial', string='Editorial')